const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('orders.json');
const middlewares = jsonServer.defaults();

const port = process.env.PORT || 3001;

server.use(middlewares);
server.use(router);
if(!router){
  console.log("Erro");
} else {
  console.log("Ok");
}
server.listen(port, () => {
  console.log(`JSON Server running in ${port}`);
});