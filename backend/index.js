const express = require('express');
const app = express();
const fs = require('fs');
const cors = require('cors');

app.use(express.json());
app.use(cors(
  //origin
  //'http://localhost:3000'
  'https://nossa-pizza.netlify.app/'
));

//Ver todos os clientes
app.get('/clients', (_, res) => {
  fs.readFile('clients.json', 'utf-8', (err, data) => {
    if(!err){
      res.send(data);
    }else{
      res.send(err.message);
    }
  });
});

//Cadastrar cliente
app.post('/cadastrarcli', (req, res) => {
  let client = req.body;
  fs.readFile('clients.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);
        client = {id: json.cpf, ...client};
        json.clients.push(client);

        fs.writeFile('clients.json', JSON.stringify(json), err => {
          if(err){
            console.log(err);
          } else {
            res.send('Cliente cadastrado!');
          }
        });
      } catch(err) {
        res.status(400).send(err.message);
      }
    }else{
      res.status(400).send(err.message);
    }
  })
});

//Ver todos os pedidos
app.get('/orders', (_, res) => {
  fs.readFile('orders.json', 'utf-8', (err, data) => {
    if(!err){
      res.send(data);
    } else {
      res.send(err.message);
    }
  });
});

//Registrar pedidos
app.post('/cadastrarped', (req, res) => {
  let order = req.body;
  order.icon = "⭕️"; 
  order.status = 'open';  
  fs.readFile('orders.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);
        order = {id: json.nextOrderId++, ...order};
        json.orders.push(order);

        fs.writeFile('orders.json', JSON.stringify(json), err => {
          if(err){
            console.log(err);
          } else {
            res.send('Pedido registrado!');
          }
        });
      } catch (err){
        res.status(400).send('Erro ao registrar pedido');
      }
    } else {
      res.status(400).send('Erro na leitura');
    }
  });
});

//Atualizar status dos pedidos
app.put('/novostatus/:id', (req, res) => {
  let status = req.body;

  fs.readFile('orders.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let order = JSON.parse(data);
        let orderObj = order.orders.filter(o => o.id === parseInt(req.params.id))
        orderObj[0].status = status.status;
        let statusObj = status.statuses.find(s => s.status === orderObj[0].status);
        orderObj[0].icon = statusObj.icon;
    
        fs.writeFile('orders.json', JSON.stringify(order), err => {
          if(err){
            console.log(err);
          } else{
            res.send("Status do pedido atualizado");
          }
        })

      }catch (err){
        res.send(err.message);
      }
    }
  })
  //res.send("ok")
});


//Consultar pedidos
app.get('/orders/:type/:value', (req, res) => {
  const type = req.params.type;
  console.log(type);
  fs.readFile('orders.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);
        if(type === "id"){
          let orderObj = json.orders.filter(order => order.id === parseInt(req.params.value));
          console.log(orderObj);
          res.send (orderObj);
        } else {
          let orderObj = json.orders.filter(order => order.clientId === req.params.value);
          console.log(orderObj);
          res.send(orderObj);
        }
      } catch (err){

      }
    }
  })
});

app.listen(3001, function(){
  console.log("api started");

    // Verifica se há registro de clientes. Se não houver, um será
  // criado a partir do primeiro cadastro.

  try{
    fs.readFile('clients.json', 'utf-8', (err, data) => {
      if(err){
        const initialJson = {
          clients: [],
        };
        fs.writeFile('clients.js', JSON.stringify(initialJson), err => {
          console.log(err);
        });
      }
    })
  }catch (err){
    console.log(err);
  };

  try{
    fs.readFile('orders.json', 'utf-8', (err, data) => {
      if(err) {
        const initialJson = {
          nextOrderId: 1,
          orders: []
        };
        fs.writeFile('orders.json', JSON.stringify(initialJson), err => {
          console.log(err);
        })
      }
    });
  } catch (err) {
    console.log(err);
  }

  try{
    fs.readFile('data.json', 'utf-8', (err, data) => {
      if(err){
        const initialJson = {
          lanes: [],
        };
        fs.writeFile('data.json', JSON.stringify(initialJson), err => {
          console.log(err);
        });
      }
    })
  }catch (err){
    console.log(err);
  };
});
